console.log("Hello, world!");
console. log ("Hello, Batch 203");
console.
log
("Hello, everyone!");
// This is a single line comment
/* 
	This is a Multi line comment 
*/

// Syntax and Statements
// Statements in programming are instructions that we tell the computer to perform.

// Syntax in programming, it is a set of rules that describes how stattements must be structed.

// Variables - it is used to contain data
/* Syntax in declaring variables
	- let/const variableName;
	const are varibles that has fixed value.
	let - are variables that changes its value.
*/

let myVariable = "Hello Ciara!";
console.log(myVariable);

/*
	console.log(hello);
	let hello;

	Guides in writing variables:
	1. Use the 'let' keyword followed by the variables name of your choosing and use the assignment operator (=) to assign a value.
	2. Variable names should start with a lowercase character, use camelCase for multiple wordss.
	3. For constant variables, use the 'const' keyword.
	4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.
	5. Never name a variable starting with numbers.
*/

/*
	Declaring a variable with initial value
	let/const variableName = value;
*/

let productName = 'desktop computer';
console.log(productName);

let product = "Alvin's computer";
console.log(product);

let productPrice = 1999;
console.log(productPrice);

const interest = 3.539;

// let - usually used to change/reassign the values in our variables.
// Reassigning variable values
// Syntax
	//variableName = newValue

productName = "Laptop";
console.log(productName);

let friend = "Kate";
friend = "Jane";
// let friend = "Jane"; error: Identifier "friend" has already been declared.
console.log(friend);  

console.log(interest);

//Reassigning variables Vs Initializing variables.
// Declare ng variable
let supplier;
// Initialization is done after the variables has been declared.
supplier = "John Smith Trading"
console.log(supplier);

// Reassignment of variable because it's initial value was already declared.
supplier = "Zuitt Store";
console.log(supplier);

/*	Error due to const initialization
	const pi 
	pi = 3.1416;
	console.log(pi); 
*/

const pi = 3.1416;
console.log(pi);

/*
	var(ES1) vs let/const (ES6)

*/

// Multiple variable declaration
let productCode = "DC017";
const productBrand = "Dell";
console.log(productCode, productBrand);

// Using a variable with reserved words
// const let = "Hello";
// console.log(let); // error: cannot used reserved keyword as a variable name.

// [SECTION] Data Types
// Strings 
// Strings are series of characters that create a word, a phrase, a sentence or anything related to creating text.
// Strings in Javascript a single ('') or double ("") quote.
let country = 'Philippines';
let province = "Metro Manila";

// Concatenation Strings
// Multiple string values can be combined to create a single string using the "+" symbol.
let fullAddress = province + ', ' + country;
console.log(fullAddress);

let greeting = "I live in the ";
console.log(greeting + country);

let mailAddress = "Metro Manila\nPhilippines"
console.log(mailAddress);

let message = "John's employees went home early";
console.log(message);

message = 'John\'s employees went home early';
console.log(message); 

// Numbers
// Integers/Whole Numbers
let headcount = 26;
console.log(headcount);

// Decimal Numbers/Fraction
let grade = 98.7;
console.log(grade);

//Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combine number and strings
console.log("John's grade last quarter is " + grade);

// Boolean
// True/False
let isMarried = false;
let isGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + isGoodConduct);

// Array
// It is used to store multiple values with similar data type.
// Syntax:
	// let/const arrayName = [elementA, elementB, elementC,...]
	let grades = [98.7, 92.1, 90.2, 94.6];
	console.log(grades);

	// different data types
	// Storing different data types inside an array is not recommended because it will not make sense to in the context of programming.
	let details = ["John", "Smith", 32, true];
	console.log(details);

// Objects 
// Objects are another special kind of data types that's used to mimic real world objects/items.

// Syntax 
	/*
		let/const objectName = {
			propertyA: value,
			propertyB: value
		}
	*/

	let person = {
		fullName: "Juan Dela Cruz",
		age: 35,
		isMarried: false,
		contact: ["+63917 123 4567"],
		address: {
			houseNumber: "345",
			city: "Manila"
		}
	}

	console.log(person);

	// Create abstract object
	let myGrades = {
		firstGrading: 98.7,
		secondGrading: 92.1,
		thirdGrading: 90.2,
		fourthGrading: 94.6,
	}
	console.log(myGrades);

	console.log(typeof myGrades);

	console.log(typeof grades);

	// Constant Objects and Arrays
	const anime = ["one piece", "one punch man", "attack on titan"];
	anime[0] = "kimetsu no yaiba";

	console.log(anime);

	// Null
	// It is used to intentionally express the absence of the value in a variable declaration/initialization.

	let spouse = null;
	console.log(spouse);

	let myNumber = 0; // Number
	let mystring = ""; // String

	// Undefined
	// Represent the state of a variable that has been declared 